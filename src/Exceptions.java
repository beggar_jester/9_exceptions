import java.util.Scanner;

public class Exceptions {
    public static void main(String[] args) {
        System.out.println("Enter a text with words without more than 10 latin letters:");
        String inputString = new Scanner(System.in).nextLine();
        String[] stringToArray = inputString.split(" ");
        for (String currentWord : stringToArray) {
            try {
                if (lettersCounter(currentWord) > 10) {
                    System.err.print(currentWord + ": ");
                    throw new InputException("Word contains more than 10 latin letters");
                }
            } catch (InputException ignored) {
            }
        }
    }

    private static int lettersCounter(String toCheck) {
        int count = 0;
        for (int i = 0; i < toCheck.length(); i++) {
            if (("" + toCheck.charAt(i)).matches("^[a-zA-Z]+$")) count++;
        }
        return count;
    }
}